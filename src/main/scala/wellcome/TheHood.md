# Ubytovaní

Z rozpočtových důvodů nejsme schopni hradit ubytování všem zvaným,
přesto bychom Vám alespoň rádi usnadnili hledání ubytování.

Proto máme pro svatební hosty předběžně rezervovaný **hostel** v Zámeckém pivovaru v Litomyšli,
pokud byste byli rádi ubytovaní zde, prosíme napište nám
a my vám v pravý čas zašleme heslo, pod kterým Vám bude umožněna rezervace.
https://esclitomysl.cz/rezervace/typyPokoju

Pokud byste preferovali jiné ubytování, litomyšl skýtá spoustu možností.


Například ubytování v [**hotelu**](https://esclitomysl.cz/rezervace/typyPokoju) v Zámeckém pivovaru kde budete poblíž ostatních,
nebo třeba [zde](www.tasner.cz) a [zde](www.zlatahvezda.com).

Další možnosti jsou již na Vás.

[zpět na hlavní stránku](./IntroPage.md)