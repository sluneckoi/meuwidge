Jak jste si možná všimli při pročítání informací k obřadu,
během mše se budou číst tři texty z Bible. I na našem oznámení jste si
mohli povšimnout krátké věty:
> ... sviťte jako hvězdy, které osvěcují svět ...

Tento úryvek pochází ze druhého čtení, které na naší svatbě zazní.

Prvním čtením které zazní je text:

>Bůh řekl: "Učiňme člověka jako náš obraz, podle naší podoby.
Ať vládne nad mořskými rybami, nad nebeským ptactvem,
nad krotkými zvířaty, divokou zvěří a nad veškerou drobnou zvířenou,
 která se pohybuje po zemi." Bůh stvořil člověka jako svůj obraz,
 jak obraz Boží ho stvořil, stvořil je jako muže a ženu.
 Bůh jim požehnal slovy: "Ploďte a množte se, naplňte zemi a podmaňte si ji!
 Vládněte nad rybami moře, nad ptactvem nebe i nade všemi živočichy,
 kteří se pohybují po zemi." Bůh viděl všechno, co udělal, a hle
 - bylo to velmi dobré.


Odkaz který jsme ale na oznámení uváděli je výrazně delší, než věta
pod kterou je napsán. Celý text zní:

> Je-li možno povzbudit v Kristu, je-li možno posílit láskou, je-li jaké společenství Ducha, je-li jaký soucit a slitování:
  dovršte mou radost a buďte stejné mysli, mějte stejnou lásku, buďte jedné duše, jednoho smýšlení,
  v ničem se nedejte ovládat ctižádostí ani ješitností, nýbrž v pokoře pokládejte jeden druhého za přednějšího než sebe;
  každý ať má na mysli to, co slouží druhým, ne jen jemu.
  Nechť je mezi vámi takové smýšlení jako v Kristu Ježíši:
  Způsobem bytí byl roven Bohu, a přece na své rovnosti nelpěl,
  nýbrž sám sebe zmařil, vzal na sebe způsob služebníka, stal se jedním z lidí. A v podobě člověka
  se ponížil, v poslušnosti podstoupil i smrt, a to smrt na kříži.
  Proto ho Bůh vyvýšil nade vše a dal mu jméno nad každé jméno,
  aby se před jménem Ježíšovým sklonilo každé koleno – na nebi, na zemi i pod zemí –
  a k slávě Boha Otce každý jazyk aby vyznával: Ježíš Kristus jest Pán.
  A tak, moji milí, jako jste vždycky byli poslušní – nikoli jen v mé přítomnosti, ale nyní mnohem více v mé nepřítomnosti – s bázní a chvěním uvádějte ve skutek své spasení.
  Neboť je to Bůh, který ve vás působí, že chcete i činíte, co se mu líbí.
  Všechno dělejte bez reptání a bez pochybování,
  abyste byli bezúhonní a ryzí, Boží děti bez poskvrny uprostřed pokolení pokřiveného a zvráceného.
  **V něm sviťte jako hvězdy, které osvěcují svět**,
  držte se slova života, abych se vámi mohl pochlubit v den Kristův, že jsem nadarmo neběžel ani se nadarmo nenamáhal.

Ačkoliv se jedná o list psaný do Filip, věříme, že se netýká pouze vztahů uvnitř církve,
ale že je možné jej vztáhnout i na vztah mezi mužem a ženou.

S tím souzní i třetí čtení:

>"Vy jste sůl země. Jestliže však sůl ztratí chuť, čím bude osolena?
 K ničemu se už nehodí, než aby se vyhodila ven a lidé po ní šlapali.
 Vy jste světlo světa. Nemůže se skrýt město položené na hoře.
 A když se svítilna rozsvítí, nestaví se pod nádobu, ale na podstavec,
 takže svítí všem v domě. Tak ať vaše světlo svítí lidem,
 aby viděli vaše dobré skutky a velebili vašeho Otce v nebesích."

Nerozumíme těmto čtením jako našemu současnému stavu.
Rozumíme jim jako příkazu a pozvání.
K tomuto nelehkému cíli bychom společně rádi směřovali a budeme rádi,
budete-li nám v tom pomocí, aby naše společné jméno mohlo být více,
než jenom shluk hlásek na občance.

[zpět na hlavní stránku](./IntroPage.md)