Pokud jste cekali, ze zde naleznete nejake fotky zenicha a nevesty,
cekali jste, jak vidno, spatne. Presto prosim doctete par nasledujicich radku.

Nase svatba neni svetskym obradem, ale obradem cirkevnim, a proto
je pro nas okamzik samotny cennejsi, nez uchovani hmotne pamatky na nej.
Z tohoto duvodu mame pouze fotografa, nikoliv vsak kameramana, abychom nenarusovali
prubeh mse vice, nez je nutne.

Durazne vas proto zadame abyste i Vy venovali behem obradu pozornost
predevsim obradu samotnemu, a ne porizovani zaznamu. Pokud by vas prece
jenom lakalo nejaky ten zaznam si poridit, opatrete si jej, prosime, zpusobem,
ktery nebude rusit ani nas ani ostatni ve vasem okoli.

Co to znamena?


 * budete-li porizovat zaznam ve forme videa, ucinte tak vyhradne z prostoru kuru.

 * budete-li porizovat zaznam ve forme fotografie, potom drive nez zmacknete spoust,
 trikrat se ujistete, ze mate vypnuty blesk a prosime omezte svuj pohyb na minimum

 * budete-li porizovat zaznam ve forme rucne psanych poznamek, piste prosime rychle a potichu.

 * pokud se Vam libi prostor kostela jako takovy,
 budete mit dostatek casu zaznamenat si jej po obrade

pokud jste docetli az sem, za odmenu vam ukazeme par fotek, ktere jste zde hledali.
lze je nalezt [ZDE](./galery/All.md)

[zpět na hlavní stránku](./IntroPage.md)